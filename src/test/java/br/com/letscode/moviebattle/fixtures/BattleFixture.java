package br.com.letscode.moviebattle.fixtures;

import br.com.letscode.moviebattle.model.Battle;
import br.com.letscode.moviebattle.model.BattleStatus;

public class BattleFixture {

    public static class WithIdAndUserId {
        private String battleId;
        private String userId;

        public WithIdAndUserId(String battleId, String userId) {
            this.battleId = battleId;
            this.userId = userId;
        }

        public Battle build() {
            return new Battle.Builder().id(battleId).userId(userId).build();
        }
    }

    public static class AsFinishedForUserId {
        private String userId;

        public AsFinishedForUserId(String userId) {
            this.userId = userId;
        }

        public Battle build() {
            return new Battle.Builder().status(BattleStatus.FINISHED).userId(userId).build();
        }
    }

    public static class AsStartedForUserId {
        private String userId;

        public AsStartedForUserId(String userId) {
            this.userId = userId;
        }

        public Battle build() {
            return new Battle.Builder().status(BattleStatus.STARTED).userId(userId).build();
        }
    }
}
