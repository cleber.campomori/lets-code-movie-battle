package br.com.letscode.moviebattle.fixtures;

import br.com.letscode.moviebattle.model.Round;
import br.com.letscode.moviebattle.model.RoundOption;

import java.util.UUID;

public class RoundFixture {

    public static class WithBattleId {
        private String battleId;
        private String userId;

        public WithBattleId(String battleId) {
            this.battleId = battleId;
        }

        public Round build() {
            String id = UUID.randomUUID().toString();
            return new Round.Builder()
                    .id(id)
                    .battleId(userId)
                    .options(
                            new RoundOption.Builder()
                                    .roundId(id)
                                    .name("Foo")
                                    .description("Desc foo")
                                    .isRight(true)
                                    .build(),
                            new RoundOption.Builder()
                                    .roundId(id)
                                    .name("Bar")
                                    .description("Desc bar")
                                    .isRight(false)
                                    .build()
                    )
                    .build();
        }
    }
}
