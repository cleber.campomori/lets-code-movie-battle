package br.com.letscode.moviebattle.fixtures;

public class UserFixture {
    public static String USER_ID_WITHOUT_BATTLES = "A-ID";
    public static String USER_ID_WITH_BATTLES = "OTHER-ID";
    public static String USER_ID_WITH_CLOSED_BATTLES = "CLOSED-OTHER-ID";
    public static String USER_ID_WITH_STARTED_BATTLES = "STARTED-OTHER-ID";

}
