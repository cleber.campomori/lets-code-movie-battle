package br.com.letscode.moviebattle.service.battle;

import br.com.letscode.moviebattle.exception.BattleAlreadyExistsForUserException;
import br.com.letscode.moviebattle.fixtures.BattleFixture;
import br.com.letscode.moviebattle.model.Battle;
import br.com.letscode.moviebattle.repository.BattleRepository;
import br.com.letscode.moviebattle.repository.RoundRepository;
import br.com.letscode.moviebattle.service.BattleService;
import br.com.letscode.moviebattle.thirdparty.movie.MovieFetcherGateway;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;

import java.util.List;
import java.util.UUID;

import static br.com.letscode.moviebattle.fixtures.UserFixture.USER_ID_WITHOUT_BATTLES;
import static br.com.letscode.moviebattle.fixtures.UserFixture.USER_ID_WITH_BATTLES;
import static br.com.letscode.moviebattle.fixtures.UserFixture.USER_ID_WITH_CLOSED_BATTLES;
import static br.com.letscode.moviebattle.fixtures.UserFixture.USER_ID_WITH_STARTED_BATTLES;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BattleServiceBattleCreationTests {

    private BattleService battleService;
    private BattleRepository battleRepository;
    private Battle fakeOpenBattle = new BattleFixture
            .WithIdAndUserId(UUID.randomUUID().toString(), USER_ID_WITH_BATTLES)
            .build();
    private Battle fakeClosedBattle = new BattleFixture.AsFinishedForUserId(USER_ID_WITH_CLOSED_BATTLES).build();
    private Battle fakeStartedBattle = new BattleFixture.AsStartedForUserId(USER_ID_WITH_BATTLES).build();

    @BeforeAll
    public void before() {
        setupBattleRepository();
        battleService = new BattleService(battleRepository, Mockito.mock(RoundRepository.class),
                Mockito.mock(MovieFetcherGateway.class), 2, 1);
    }

    @Test
    public void battleCanBeCreatedForUserWithoutBattles() {
        Assertions.assertDoesNotThrow(() -> {
            battleService.startBattleForUser(USER_ID_WITHOUT_BATTLES);
        });
    }

    @Test
    public void battleCannotBeCreatedForUserWithOpenBattles() {
        Assertions.assertThrows(BattleAlreadyExistsForUserException.class, () -> {
            battleService.startBattleForUser(USER_ID_WITH_BATTLES);
        });
    }

    @Test
    public void battleCannotBeCreatedForUserWithStartedBattles() {
        Assertions.assertThrows(BattleAlreadyExistsForUserException.class, () -> {
            battleService.startBattleForUser(USER_ID_WITH_STARTED_BATTLES);
        });
    }

    @Test
    public void battleCanBeCreatedForUserWithClosedBattles() {
        Assertions.assertDoesNotThrow(() -> {
            battleService.startBattleForUser(USER_ID_WITH_CLOSED_BATTLES);
        });
    }

    private void setupBattleRepository() {
        battleRepository = Mockito.mock(BattleRepository.class);
        Mockito.when(battleRepository.insert(Mockito.any(Battle.class))).thenAnswer(a -> a.getArgument(0));
        Mockito.when(battleRepository.update(Mockito.any(Battle.class))).thenAnswer(a -> a.getArgument(0));
        Mockito.doReturn(List.of()).when(battleRepository).battlesForUser(USER_ID_WITHOUT_BATTLES);
        Mockito.doReturn(List.of(fakeOpenBattle)).when(battleRepository).battlesForUser(USER_ID_WITH_BATTLES);
        Mockito.doReturn(List.of(fakeClosedBattle)).when(battleRepository).battlesForUser(USER_ID_WITH_CLOSED_BATTLES);
        Mockito.doReturn(List.of(fakeStartedBattle)).when(battleRepository).battlesForUser(USER_ID_WITH_STARTED_BATTLES);

    }

}
