package br.com.letscode.moviebattle.thirdparty.movie.impl;

import br.com.letscode.moviebattle.model.Movie;
import br.com.letscode.moviebattle.thirdparty.movie.MovieFetcherStrategy;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

@Component
public class TheMovieDbMovieFetcherStrategy implements MovieFetcherStrategy {

    private String apiKey;
    private String baseRoute = "https://api.themoviedb.org/3/search/movie";
    private RestTemplate template;

    @Autowired
    public TheMovieDbMovieFetcherStrategy(
            RestTemplateBuilder templateBuilder,
            @Value("${movie.fetcher.themoviedatabase.apikey}")
            String apiKey
    ) {
        this.template = templateBuilder.build();
        this.apiKey = apiKey;
    }

    @Override
    public Movie fetchRandomMovie() {
        String searchArgument = RandomStringUtils.random(1, true, false);
        TheMovieDbRootResponse root =
                template.getForObject(String.format("%s?query=%s&page=1&api_key=%s", baseRoute, searchArgument, apiKey),
                        TheMovieDbRootResponse.class
                );
        TheMovieDbMovieResponse pickedMovie = pickRandomMovieFromResponse(root);
        return asMovie(pickedMovie);
    }

    private TheMovieDbMovieResponse pickRandomMovieFromResponse(TheMovieDbRootResponse root) {
        return root.getResults().get(RandomUtils.nextInt(0, root.getResults().size() - 1));
    }

    private Movie asMovie(TheMovieDbMovieResponse pickedMovie) {
        return new Movie(pickedMovie.getTitle(), pickedMovie.getOverview(), pickedMovie.getVoteCount(),
                new BigDecimal(pickedMovie.getVoteAverage()));
    }
}
