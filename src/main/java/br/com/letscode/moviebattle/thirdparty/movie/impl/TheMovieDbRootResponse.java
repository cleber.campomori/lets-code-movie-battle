package br.com.letscode.moviebattle.thirdparty.movie.impl;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

class TheMovieDbRootResponse {
    private int page;
    private ArrayList<TheMovieDbMovieResponse> results;
    @JsonProperty("total_pages")
    private int totalPages;
    @JsonProperty("total_results")
    private int totalResults;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public ArrayList<TheMovieDbMovieResponse> getResults() {
        return results;
    }

    public void setResults(ArrayList<TheMovieDbMovieResponse> results) {
        this.results = results;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }
}
