package br.com.letscode.moviebattle.thirdparty.movie;

import br.com.letscode.moviebattle.model.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class MovieFetcherGateway {
    private MovieFetcherStrategy movieFetcherStrategy;

    @Autowired
    public MovieFetcherGateway(MovieFetcherStrategy movieFetcherStrategy) {
        this.movieFetcherStrategy = movieFetcherStrategy;
    }

    public List<Movie> fetchRandomMovies(int quantityOfMovies) {
        return IntStream.rangeClosed(0, quantityOfMovies - 1)
                .mapToObj(n -> movieFetcherStrategy.fetchRandomMovie())
                .collect(Collectors.toList());
    }
}
