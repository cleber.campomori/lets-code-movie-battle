package br.com.letscode.moviebattle.thirdparty.movie;

import br.com.letscode.moviebattle.model.Movie;

public interface MovieFetcherStrategy {
    public Movie fetchRandomMovie();
}
