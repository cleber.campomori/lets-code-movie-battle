package br.com.letscode.moviebattle.model;

public enum BattleStatus {
    CREATED,
    STARTED,
    FINISHED
}
