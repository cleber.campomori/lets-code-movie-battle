package br.com.letscode.moviebattle.model;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public class Round {
    private String id;
    private String battleId;
    private RoundStatus status;
    private LocalDateTime createdAt;
    private Optional<LocalDateTime> answeredAt;
    private Set<RoundOption> options;

    private Round(String id, String battleId, RoundStatus status, LocalDateTime createdAt,
                  Optional<LocalDateTime> answeredAt, Set<RoundOption> options
    ) {
        this.id = id;
        this.battleId = battleId;
        this.status = status;
        this.createdAt = createdAt;
        this.answeredAt = answeredAt;
        this.options = options;
    }

    public String getId() {
        return id;
    }

    public String getBattleId() {
        return battleId;
    }

    public RoundStatus getStatus() {
        return status;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public Optional<LocalDateTime> getAnsweredAt() {
        return answeredAt;
    }

    public Set<RoundOption> getOptions() {
        return options;
    }

    public void registerOptions(RoundOption... options) {
        this.options.addAll(Set.of(options));
    }

    public void setAsCorrectlyAnswered() {
        this.answeredAt = Optional.of(LocalDateTime.now());
        this.status = RoundStatus.ANSWERED_CORRECTLY;
    }

    public void setAsIncorrectlyAnswered() {
        this.answeredAt = Optional.of(LocalDateTime.now());
        this.status = RoundStatus.ANSWERED_INCORRECTLY;
    }

    public static class Builder {
        private String id = UUID.randomUUID().toString();
        private String battleId;
        private RoundStatus status = RoundStatus.OPEN;
        private LocalDateTime createdAt = LocalDateTime.now();
        private Optional<LocalDateTime> answeredAt = Optional.empty();
        private Set<RoundOption> options = new HashSet<>();

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder battleId(String battleId) {
            this.battleId = battleId;
            return this;
        }

        public Builder status(RoundStatus status) {
            this.status = status;
            return this;
        }

        public Builder createdAt(LocalDateTime createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public Builder answeredAt(LocalDateTime answeredAt) {
            this.answeredAt = Optional.ofNullable(answeredAt);
            return this;
        }

        public Builder options(RoundOption... options) {
            this.options = Set.of(options);
            return this;
        }

        public Round build() {
            return new Round(id, battleId, status, createdAt, answeredAt, options);
        }
    }

    public static Round newEmptyRoundForBattle(String battleId) {
        return new Round.Builder().battleId(battleId).build();
    }

}
