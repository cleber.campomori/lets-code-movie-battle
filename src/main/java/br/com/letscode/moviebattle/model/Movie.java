package br.com.letscode.moviebattle.model;

import java.math.BigDecimal;

public class Movie {
    private String name;
    private String description;
    private int numberOfVotes;
    private BigDecimal rate;

    public Movie(String name, String description, int numberOfVotes, BigDecimal rate) {
        this.name = name;
        this.description = description;
        this.numberOfVotes = numberOfVotes;
        this.rate = rate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumberOfVotes() {
        return numberOfVotes;
    }

    public void setNumberOfVotes(int numberOfVotes) {
        this.numberOfVotes = numberOfVotes;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }
}
