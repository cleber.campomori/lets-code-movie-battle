package br.com.letscode.moviebattle.model;

public enum RoundStatus {
    OPEN,
    ANSWERED_CORRECTLY,
    ANSWERED_INCORRECTLY
}
