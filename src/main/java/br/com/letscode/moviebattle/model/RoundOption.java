package br.com.letscode.moviebattle.model;

import java.util.UUID;

public class RoundOption {
    private String id;
    private String name;
    private String description;
    private String roundId;
    private boolean right;

    private RoundOption(String id, String name, String description, String roundId, boolean right) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.roundId = roundId;
        this.right = right;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getRoundId() {
        return roundId;
    }

    public boolean isRight() {
        return right;
    }

    public static class Builder {

        private String id = UUID.randomUUID().toString();
        private String name = "";
        private String description = "";
        private String roundId;
        private boolean right;

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder roundId(String roundId) {
            this.roundId = roundId;
            return this;
        }

        public Builder isRight(boolean isRight) {
            this.right = isRight;
            return this;
        }

        public RoundOption build() {
            return new RoundOption(this.id, this.name, this.description, this.roundId, this.right);
        }
    }
}
