package br.com.letscode.moviebattle.model;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

public class Battle {

    private String id;
    private LocalDateTime createdAt;
    private Optional<LocalDateTime> startedAt;
    private String userId;
    private int numberOfErrors;
    private BattleStatus status;

    private Battle(String id, LocalDateTime createdAt, Optional<LocalDateTime> startedAt, String userId,
                   int numberOfErrors, BattleStatus battleStatus
    ) {
        this.id = id;
        this.createdAt = createdAt;
        this.startedAt = startedAt;
        this.userId = userId;
        this.status = battleStatus;
        this.numberOfErrors = numberOfErrors;
    }

    public String getId() {
        return id;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public Optional<LocalDateTime> getStartedAt() {
        return startedAt;
    }

    public String getUserId() {
        return userId;
    }

    public int getNumberOfErrors() {
        return numberOfErrors;
    }

    public BattleStatus getStatus() {
        return status;
    }

    public void increaseNumberOfErrors() {
        this.numberOfErrors++;
    }

    public void setAsFinished() {
        this.status = BattleStatus.FINISHED;
    }

    public void setAsStarted() {
        this.startedAt = Optional.of(LocalDateTime.now());
        this.status = BattleStatus.STARTED;
    }

    public static class Builder {
        private String id = UUID.randomUUID().toString();
        private LocalDateTime createdAt = LocalDateTime.now();
        private Optional<LocalDateTime> startedAt = Optional.empty();
        private String userId = "";
        private int numberOfErrors = 0;
        private BattleStatus status = BattleStatus.CREATED;

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder createdAt(LocalDateTime createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public Builder startedAt(LocalDateTime startedAt) {
            this.startedAt = Optional.ofNullable(startedAt);
            return this;
        }

        public Builder userId(String userId) {
            this.userId = userId;
            return this;
        }

        public Builder numberOfErrors(int numberOfErrors) {
            this.numberOfErrors = numberOfErrors;
            return this;
        }

        public Builder status(BattleStatus status) {
            this.status = status;
            return this;
        }

        public Battle build() {
            return new Battle(id, createdAt, startedAt, userId, numberOfErrors, status);
        }

    }

    public static Battle newEmptyBattleForUser(String userId) {
        return new Builder().userId(userId).build();
    }

}
