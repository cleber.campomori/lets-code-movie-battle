package br.com.letscode.moviebattle.model;

import java.util.UUID;

public class MovieBattleUser {

    private String id;
    private String username;
    private String password;

    private MovieBattleUser(String id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public static class Builder {
        private String id = UUID.randomUUID().toString();
        private String username = "";
        private String password = "";

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder username(String username) {
            this.username = username;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public MovieBattleUser build() {
            return new MovieBattleUser(id, username, password);
        }
    }
}
