package br.com.letscode.moviebattle.controller.v1.docs;

import br.com.letscode.moviebattle.api.v1.battle.BattleResponse;
import br.com.letscode.moviebattle.api.v1.error.MovieBattleErrorResponse;
import br.com.letscode.moviebattle.api.v1.round.RoundAnswerRequest;
import br.com.letscode.moviebattle.api.v1.round.RoundAnswerResponse;
import br.com.letscode.moviebattle.api.v1.round.RoundResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

public interface BattleControllerSpec {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Creates a new battle for current user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "The information about created battle", content = {
                    @Content(schema = @Schema(implementation = BattleResponse.class))
            }),
            @ApiResponse(responseCode = "409", description = "There is a running battle for current user", content = {
                    @Content(schema = @Schema(implementation = MovieBattleErrorResponse.class))
            }),
            @ApiResponse(responseCode = "500", description = "Generic internal server error", content = {
                    @Content(schema = @Schema(implementation = MovieBattleErrorResponse.class))
            })
    })
    BattleResponse startBattle();

    @DeleteMapping("/{battleId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Ends the specified battle")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "The battle is ended successfully"),
            @ApiResponse(responseCode = "412", description = "The specified battle is already finished", content = {
                    @Content(schema = @Schema(implementation = MovieBattleErrorResponse.class))
            }),
            @ApiResponse(responseCode = "500", description = "Generic internal server error", content = {
                    @Content(schema = @Schema(implementation = MovieBattleErrorResponse.class))
            })
    })
    void endBattle(@PathVariable String battleId);

    @PostMapping("/{battleId}/round")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Creates a new round for specified battle")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "The battle is ended successfully"),
            @ApiResponse(responseCode = "412", description = "The specified battle is already finished or there is a current open round for specified battle", content = {
                    @Content(schema = @Schema(implementation = MovieBattleErrorResponse.class))
            }),
            @ApiResponse(responseCode = "500", description = "Generic internal server error", content = {
                    @Content(schema = @Schema(implementation = MovieBattleErrorResponse.class))
            })
    })
    RoundResponse startRound(@PathVariable String battleId);

    @PostMapping("/{battleId}/round/{roundId}/answer")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Send an answer for the specified round of the specified battle")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The battle answer was sent and processed"),
            @ApiResponse(responseCode = "403", description = "The specified battle does not belong to current user", content = {
                    @Content(schema = @Schema(implementation = MovieBattleErrorResponse.class))
            }),
            @ApiResponse(responseCode = "412", description = "The specified battle is already finished or the specified round was already answered", content = {
                    @Content(schema = @Schema(implementation = MovieBattleErrorResponse.class))
            }),
            @ApiResponse(responseCode = "417", description = "The specified round does not belongs to specified battle", content = {
                    @Content(schema = @Schema(implementation = MovieBattleErrorResponse.class))
            }),
            @ApiResponse(responseCode = "500", description = "Generic internal server error", content = {
                    @Content(schema = @Schema(implementation = MovieBattleErrorResponse.class))
            })
    })
    RoundAnswerResponse answerRound(
            @PathVariable
            String battleId,
            @PathVariable
            String roundId,
            @RequestBody RoundAnswerRequest roundAnswerRequest
    );
}
