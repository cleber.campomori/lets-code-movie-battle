package br.com.letscode.moviebattle.controller.v1;

import br.com.letscode.moviebattle.api.v1.battle.BattleResponse;
import br.com.letscode.moviebattle.api.v1.round.RoundAnswerRequest;
import br.com.letscode.moviebattle.api.v1.round.RoundAnswerResponse;
import br.com.letscode.moviebattle.api.v1.round.RoundResponse;
import br.com.letscode.moviebattle.controller.v1.docs.BattleControllerSpec;
import br.com.letscode.moviebattle.model.Round;
import br.com.letscode.moviebattle.service.BattleService;
import br.com.letscode.moviebattle.service.arg.IsRightAnswerForRoundArg;
import br.com.letscode.moviebattle.service.result.IsRightAnswerForRoundResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(
        path = "/battle",
        produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}
)
public class BattleController implements BattleControllerSpec {

    private BattleService battleService;

    @Autowired
    public BattleController(BattleService battleService) {
        this.battleService = battleService;
    }

    @Override
    public BattleResponse startBattle() {
        return BattleResponse.from(battleService.startBattleForUser(fetchCurrentUsername()));
    }

    @Override
    public void endBattle(String battleId) {
        battleService.endBattle(battleId);
    }

    @Override
    public RoundResponse startRound(String battleId) {
        Round round = battleService.createRoundForBattle(battleId);
        return new RoundResponse.Builder().round(round).build();
    }

    @Override
    public RoundAnswerResponse answerRound(String battleId, String roundId, RoundAnswerRequest roundAnswerRequest) {
        IsRightAnswerForRoundResult result = battleService.isRightAnswerForRound(
                new IsRightAnswerForRoundArg.Builder()
                        .battleId(battleId)
                        .roundId(roundId)
                        .userId(fetchCurrentUsername())
                        .optionId(roundAnswerRequest.getOptionId())
                        .build()
        );
        return new RoundAnswerResponse(result.isRightAnswer());
    }

    private String fetchCurrentUsername() {
        return ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
    }

}
