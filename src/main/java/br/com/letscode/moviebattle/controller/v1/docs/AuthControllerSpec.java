package br.com.letscode.moviebattle.controller.v1.docs;

import br.com.letscode.moviebattle.api.v1.auth.AuthRequest;
import br.com.letscode.moviebattle.api.v1.auth.AuthResponse;
import br.com.letscode.moviebattle.api.v1.error.MovieBattleErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirements;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

public interface AuthControllerSpec {

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Creates a new session with a JWT token associated to specified user")
    @SecurityRequirements(value = {})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The JWT token associated with the user", content = {
                    @Content(schema = @Schema(implementation = AuthResponse.class))
            }),
            @ApiResponse(responseCode = "401", description = "User not authorized (either credentials are invalid or user is disabled)", content = {
                    @Content(schema = @Schema(implementation = MovieBattleErrorResponse.class))
            }),
            @ApiResponse(responseCode = "500", description = "Generic internal server error", content = {
                    @Content(schema = @Schema(implementation = MovieBattleErrorResponse.class))
            })
    })
    AuthResponse login(@RequestBody AuthRequest request);
}
