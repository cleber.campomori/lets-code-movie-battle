package br.com.letscode.moviebattle.controller.v1.docs;

import br.com.letscode.moviebattle.api.v1.error.MovieBattleErrorResponse;
import br.com.letscode.moviebattle.api.v1.user.UserRequest;
import br.com.letscode.moviebattle.api.v1.user.UserResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirements;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

public interface UserControllerSpec {

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Creates a user to access the application")
    @SecurityRequirements(value = {})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "The user information about the created user", content = {
                    @Content(schema = @Schema(implementation = UserResponse.class))
            }),
            @ApiResponse(responseCode = "409", description = "The specified username already exists", content = {
                    @Content(schema = @Schema(implementation = MovieBattleErrorResponse.class))
            }),
            @ApiResponse(responseCode = "500", description = "Generic internal server error", content = {
                    @Content(schema = @Schema(implementation = MovieBattleErrorResponse.class))
            })
    })
    UserResponse createUser(@RequestBody UserRequest request);
}
