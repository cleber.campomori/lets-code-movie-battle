package br.com.letscode.moviebattle.controller.v1;

import br.com.letscode.moviebattle.api.v1.user.UserRequest;
import br.com.letscode.moviebattle.api.v1.user.UserResponse;
import br.com.letscode.moviebattle.controller.v1.docs.UserControllerSpec;
import br.com.letscode.moviebattle.model.MovieBattleUser;
import br.com.letscode.moviebattle.service.UserService;
import br.com.letscode.moviebattle.service.arg.CreateUserArg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(
        path = "/user",
        produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}
)
public class UserController implements UserControllerSpec {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserResponse createUser(UserRequest request) {
        MovieBattleUser user = userService.createUser(
                new CreateUserArg.Builder()
                        .username(request.getUsername())
                        .password(request.getPassword())
                        .build()
        );
        return new UserResponse(user.getId(), user.getUsername());
    }

}
