package br.com.letscode.moviebattle.controller.v1;

import br.com.letscode.moviebattle.api.v1.auth.AuthRequest;
import br.com.letscode.moviebattle.api.v1.auth.AuthResponse;
import br.com.letscode.moviebattle.controller.v1.docs.AuthControllerSpec;
import br.com.letscode.moviebattle.service.AuthService;
import br.com.letscode.moviebattle.service.arg.LoginArg;
import br.com.letscode.moviebattle.service.result.LoginResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(
        path = "/auth",
        produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}
)
public class AuthController implements AuthControllerSpec {

    private AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @Override
    public AuthResponse login(AuthRequest request) {
        LoginResult result = authService.login(
                new LoginArg.Builder()
                        .username(request.getUsername())
                        .password(request.getPassword())
                        .build()
        );
        return new AuthResponse(result.getToken());
    }
}
