package br.com.letscode.moviebattle.controller.advice;

import br.com.letscode.moviebattle.api.v1.error.MovieBattleErrorResponse;
import br.com.letscode.moviebattle.exception.BattleAlreadyExistsForUserException;
import br.com.letscode.moviebattle.exception.BattleAlreadyOverException;
import br.com.letscode.moviebattle.exception.BattleDoesNotBelongToUserException;
import br.com.letscode.moviebattle.exception.BattleNotFoundException;
import br.com.letscode.moviebattle.exception.RoundAlreadyAnsweredException;
import br.com.letscode.moviebattle.exception.RoundAlreadyOpenForBattleException;
import br.com.letscode.moviebattle.exception.RoundDoesNotBelongToBattleException;
import br.com.letscode.moviebattle.exception.RoundNotFoundException;
import br.com.letscode.moviebattle.exception.UserAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.Tracer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class MovieBattleControllerAdvice {

    private Tracer tracer;

    @Autowired
    public MovieBattleControllerAdvice(Tracer tracer) {
        this.tracer = tracer;
    }

    @ExceptionHandler(BattleAlreadyExistsForUserException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity handleBattleAlreadyExistsException(HttpServletRequest request) {
        return new MovieBattleErrorResponse.Builder()
                .message("There is already a battle in progress for current user")
                .correlationId(tracer.currentSpan().context().traceId())
                .request(request)
                .httpStatusCode(HttpStatus.CONFLICT)
                .response();
    }

    @ExceptionHandler(BattleAlreadyOverException.class)
    @ResponseStatus(HttpStatus.PRECONDITION_FAILED)
    public ResponseEntity handleBattleAlreadyOverException(HttpServletRequest request) {
        return new MovieBattleErrorResponse.Builder()
                .message("The battle is already over")
                .correlationId(tracer.currentSpan().context().traceId())
                .request(request)
                .httpStatusCode(HttpStatus.PRECONDITION_FAILED)
                .response();
    }

    @ExceptionHandler({BattleNotFoundException.class, RoundNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity handleNotFoundException(RuntimeException ex, HttpServletRequest request) {
        return new MovieBattleErrorResponse.Builder()
                .message(ex.getMessage())
                .correlationId(tracer.currentSpan().context().traceId())
                .request(request)
                .httpStatusCode(HttpStatus.NOT_FOUND)
                .response();
    }

    @ExceptionHandler(RoundAlreadyOpenForBattleException.class)
    @ResponseStatus(HttpStatus.PRECONDITION_FAILED)
    public ResponseEntity handleRoundAlreadyOpenForBattleException(RoundAlreadyOpenForBattleException ex, HttpServletRequest request) {
        return new MovieBattleErrorResponse.Builder()
                .message(ex.getMessage())
                .correlationId(tracer.currentSpan().context().traceId())
                .request(request)
                .httpStatusCode(HttpStatus.PRECONDITION_FAILED)
                .response();
    }

    @ExceptionHandler(BattleDoesNotBelongToUserException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ResponseEntity handleBattleDoesNotBelongToUserException(BattleDoesNotBelongToUserException ex, HttpServletRequest request) {
        return new MovieBattleErrorResponse.Builder()
                .message(ex.getMessage())
                .correlationId(tracer.currentSpan().context().traceId())
                .request(request)
                .httpStatusCode(HttpStatus.FORBIDDEN)
                .response();
    }

    @ExceptionHandler(RoundDoesNotBelongToBattleException.class)
    @ResponseStatus(HttpStatus.EXPECTATION_FAILED)
    public ResponseEntity handleRoundDoesNotBelongToBattleException(RoundDoesNotBelongToBattleException ex, HttpServletRequest request) {
        return new MovieBattleErrorResponse.Builder()
                .message(ex.getMessage())
                .correlationId(tracer.currentSpan().context().traceId())
                .request(request)
                .httpStatusCode(HttpStatus.EXPECTATION_FAILED)
                .response();
    }

    @ExceptionHandler(RoundAlreadyAnsweredException.class)
    @ResponseStatus(HttpStatus.PRECONDITION_FAILED)
    public ResponseEntity handleRoundAlreadyAnsweredException(RoundAlreadyAnsweredException ex, HttpServletRequest request) {
        return new MovieBattleErrorResponse.Builder()
                .message(ex.getMessage())
                .correlationId(tracer.currentSpan().context().traceId())
                .request(request)
                .httpStatusCode(HttpStatus.PRECONDITION_FAILED)
                .response();
    }

    @ExceptionHandler(UserAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity handleBattleAlreadyExistsException(UserAlreadyExistsException ex, HttpServletRequest request) {
        return new MovieBattleErrorResponse.Builder()
                .message(ex.getMessage())
                .correlationId(tracer.currentSpan().context().traceId())
                .request(request)
                .httpStatusCode(HttpStatus.CONFLICT)
                .response();
    }

    @ExceptionHandler(AuthenticationException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ResponseEntity handleAuthenticationException(AuthenticationException ex, HttpServletRequest request) {
        return new MovieBattleErrorResponse.Builder()
                .message(ex.getMessage())
                .correlationId(tracer.currentSpan().context().traceId())
                .request(request)
                .httpStatusCode(HttpStatus.UNAUTHORIZED)
                .response();
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity handleGenericBattleNotFoundException(Exception ex, HttpServletRequest request) {
        return new MovieBattleErrorResponse.Builder()
                .message(String.format("Unexpected error [%s]", ex.getMessage()))
                .correlationId(tracer.currentSpan().context().traceId())
                .request(request)
                .httpStatusCode(HttpStatus.INTERNAL_SERVER_ERROR)
                .response();
    }
}
