package br.com.letscode.moviebattle.service.result;

public class LoginResult {
    private String token;

    public LoginResult(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
