package br.com.letscode.moviebattle.service;

import br.com.letscode.moviebattle.exception.UserAlreadyExistsException;
import br.com.letscode.moviebattle.model.MovieBattleUser;
import br.com.letscode.moviebattle.repository.UserRepository;
import br.com.letscode.moviebattle.service.arg.CreateUserArg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public MovieBattleUser createUser(CreateUserArg arg) {
        Optional<MovieBattleUser> user = userRepository.byUsername(arg.getUsername());
        if (user.isPresent()) {
            throw new UserAlreadyExistsException(arg.getUsername());
        }
        MovieBattleUser movieBattleUser = new MovieBattleUser.Builder()
                .username(arg.getUsername())
                .password(passwordEncoder.encode(arg.getPassword()))
                .build();
        userRepository.insert(movieBattleUser);
        return movieBattleUser;
    }
}
