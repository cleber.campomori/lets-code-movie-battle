package br.com.letscode.moviebattle.service;

import br.com.letscode.moviebattle.model.MovieBattleUser;
import br.com.letscode.moviebattle.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieBattleUserDetailsService implements UserDetailsService {

    private UserRepository userRepository;

    @Autowired
    public MovieBattleUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        MovieBattleUser user = userRepository.byUsername(username).orElseThrow(() ->
                new UsernameNotFoundException(String.format("User with username %s does not exists", username))
        );
        return new User(user.getUsername(), user.getPassword(), List.of());
    }
}
