package br.com.letscode.moviebattle.service.arg;

public class IsRightAnswerForRoundArg {
    private String battleId;
    private String userId;
    private String roundId;
    private String optionId;

    private IsRightAnswerForRoundArg(String battleId, String userId, String roundId, String optionId) {
        this.battleId = battleId;
        this.userId = userId;
        this.roundId = roundId;
        this.optionId = optionId;
    }

    public String getBattleId() {
        return battleId;
    }

    public String getUserId() {
        return userId;
    }

    public String getRoundId() {
        return roundId;
    }

    public String getOptionId() {
        return optionId;
    }

    public static class Builder {
        private String battleId;
        private String userId;
        private String roundId;
        private String optionId;

        public Builder battleId(String battleId) {
            this.battleId = battleId;
            return this;
        }

        public Builder userId(String userId) {
            this.userId = userId;
            return this;
        }

        public Builder roundId(String roundId) {
            this.roundId = roundId;
            return this;
        }

        public Builder optionId(String optionId) {
            this.optionId = optionId;
            return this;
        }

        public IsRightAnswerForRoundArg build() {
            return new IsRightAnswerForRoundArg(battleId, userId, roundId, optionId);
        }
    }

}
