package br.com.letscode.moviebattle.service;

import br.com.letscode.moviebattle.infra.security.Jwt;
import br.com.letscode.moviebattle.service.arg.LoginArg;
import br.com.letscode.moviebattle.service.result.LoginResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    private AuthenticationManager authenticationManager;
    private PasswordEncoder passwordEncoder;
    private Jwt jwt;

    @Autowired
    public AuthService(AuthenticationManager authenticationManager, PasswordEncoder passwordEncoder, Jwt jwt) {
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
        this.jwt = jwt;
    }

    public LoginResult login(LoginArg arg) {
        Authentication authentication =
                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(arg.getUsername(), arg.getPassword()));
        String token = jwt.generateForUsername(((User) authentication.getPrincipal()).getUsername());
        return new LoginResult(token);
    }
}
