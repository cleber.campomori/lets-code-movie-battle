package br.com.letscode.moviebattle.service;

import br.com.letscode.moviebattle.exception.BattleAlreadyExistsForUserException;
import br.com.letscode.moviebattle.exception.BattleAlreadyOverException;
import br.com.letscode.moviebattle.exception.BattleDoesNotBelongToUserException;
import br.com.letscode.moviebattle.exception.BattleNotFoundException;
import br.com.letscode.moviebattle.exception.RoundAlreadyAnsweredException;
import br.com.letscode.moviebattle.exception.RoundAlreadyOpenForBattleException;
import br.com.letscode.moviebattle.exception.RoundDoesNotBelongToBattleException;
import br.com.letscode.moviebattle.exception.RoundNotFoundException;
import br.com.letscode.moviebattle.model.Battle;
import br.com.letscode.moviebattle.model.BattleStatus;
import br.com.letscode.moviebattle.model.Movie;
import br.com.letscode.moviebattle.model.Round;
import br.com.letscode.moviebattle.model.RoundOption;
import br.com.letscode.moviebattle.model.RoundStatus;
import br.com.letscode.moviebattle.repository.BattleRepository;
import br.com.letscode.moviebattle.repository.RoundRepository;
import br.com.letscode.moviebattle.service.arg.IsRightAnswerForRoundArg;
import br.com.letscode.moviebattle.service.result.IsRightAnswerForRoundResult;
import br.com.letscode.moviebattle.thirdparty.movie.MovieFetcherGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class BattleService {

    private BattleRepository battleRepository;
    private RoundRepository roundRepository;
    private MovieFetcherGateway movieFetcher;
    private int moviesToFetchEachRound;

    private int limitOfWrongAnswers;

    @Autowired
    public BattleService(
            BattleRepository battleRepository,
            RoundRepository roundRepository,
            MovieFetcherGateway movieFetcher,
            @Value("${app.movies.tofetch}")
            int moviesToFetchEachRound,
            @Value("${app.limit.wronganswers}")
            int limitOfWrongAnswers
    ) {
        this.battleRepository = battleRepository;
        this.roundRepository = roundRepository;
        this.movieFetcher = movieFetcher;
        this.moviesToFetchEachRound = moviesToFetchEachRound;
        this.limitOfWrongAnswers = limitOfWrongAnswers;
    }

    public Battle startBattleForUser(String userId) {
        if (thereIsBattlesInProgressForUser(userId)) {
            throw new BattleAlreadyExistsForUserException(userId);
        }
        Battle newBattle = Battle.newEmptyBattleForUser(userId);
        battleRepository.insert(newBattle);
        return newBattle;
    }

    public Battle endBattle(String battleId) {
        Battle battle = getBattleById(battleId);
        if (battle.getStatus() == BattleStatus.FINISHED) {
            throw new BattleAlreadyOverException(battleId);
        }
        battle.setAsFinished();
        battleRepository.update(battle);
        return battle;
    }

    public Round createRoundForBattle(String battleId) {
        Battle battle = getBattleById(battleId);
        if (battle.getStatus() == BattleStatus.FINISHED) {
            throw new BattleAlreadyOverException(battle.getId());
        }
        Collection<Round> currentRounds = roundRepository.byBattleId(battleId);
        if (currentRounds.stream().anyMatch(round -> round.getStatus() == RoundStatus.OPEN)) {
            throw new RoundAlreadyOpenForBattleException(battleId);
        }
        Round round = generateNewRoundForBattle(battle);
        roundRepository.insert(round);
        return round;
    }

    @Transactional
    public IsRightAnswerForRoundResult isRightAnswerForRound(IsRightAnswerForRoundArg arg) {
        Battle battle = getBattleById(arg.getBattleId());
        if (!battle.getUserId().equals(arg.getUserId())) {
            throw new BattleDoesNotBelongToUserException(arg.getBattleId());
        }
        if (battle.getStatus() == BattleStatus.FINISHED) {
            throw new BattleAlreadyOverException(battle.getId());
        }
        Round round = getRoundById(arg.getRoundId());
        if (!round.getBattleId().equals(arg.getBattleId())) {
            throw new RoundDoesNotBelongToBattleException(arg.getBattleId(), arg.getRoundId());
        }
        if (round.getStatus() != RoundStatus.OPEN) {
            throw new RoundAlreadyAnsweredException(round.getId());
        }
        boolean isRightAnswerChose = round.getOptions().stream().anyMatch(option -> option.getId().equals(arg.getOptionId()) && option.isRight());
        if (!isRightAnswerChose) {
            battle.increaseNumberOfErrors();
            round.setAsIncorrectlyAnswered();
        } else {
            round.setAsCorrectlyAnswered();
        }
        if (!battle.getStartedAt().isPresent()) {
            battle.setAsStarted();
        }
        if (battle.getNumberOfErrors() == this.limitOfWrongAnswers) {
            battle.setAsFinished();
        }
        roundRepository.update(round);
        battleRepository.update(battle);
        return new IsRightAnswerForRoundResult.Builder()
                .battleId(arg.getBattleId())
                .roundId(arg.getRoundId())
                .isRightAnswer(isRightAnswerChose)
                .build();
    }

    private boolean thereIsBattlesInProgressForUser(String userId) {
        return battleRepository.battlesForUser(userId).stream()
                .anyMatch(battle -> Set.of(BattleStatus.CREATED, BattleStatus.STARTED).contains(battle.getStatus()));
    }

    private Battle getBattleById(String battleId) {
        Optional<Battle> battle = battleRepository.byId(battleId);
        return battle.orElseThrow(() -> new BattleNotFoundException(battleId));
    }

    private Round getRoundById(String roundId) {
        Optional<Round> round = roundRepository.byId(roundId);
        return round.orElseThrow(() -> new RoundNotFoundException(roundId));
    }

    private Movie getRightMovie(List<Movie> moviesForRound) {
        return moviesForRound.stream()
                .max(Comparator.comparing(movie -> new BigDecimal(movie.getNumberOfVotes()).multiply(movie.getRate())))
                .get();
    }

    private List<RoundOption> generateRoundOptionsForRound(Round round, List<Movie> moviesForRound) {
        Movie rightMovie = getRightMovie(moviesForRound);
        return moviesForRound.stream().map(movie -> new RoundOption.Builder()
                .roundId(round.getId())
                .name(movie.getName())
                .description(movie.getDescription())
                .isRight(movie.equals(rightMovie))
                .build()).collect(Collectors.toList());
    }

    private Round generateNewRoundForBattle(Battle battle) {
        Round round = Round.newEmptyRoundForBattle(battle.getId());
        List<Movie> moviesForRound = movieFetcher.fetchRandomMovies(moviesToFetchEachRound);
        round.registerOptions(generateRoundOptionsForRound(round, moviesForRound).toArray(RoundOption[]::new));
        return round;
    }
}
