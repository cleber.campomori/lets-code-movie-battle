package br.com.letscode.moviebattle.service.result;

public class IsRightAnswerForRoundResult {
    private String battleId;
    private String roundId;
    private boolean rightAnswer;

    private IsRightAnswerForRoundResult(String battleId, String roundId, boolean rightAnswer) {
        this.battleId = battleId;
        this.roundId = roundId;
        this.rightAnswer = rightAnswer;
    }

    public String getBattleId() {
        return battleId;
    }

    public String getRoundId() {
        return roundId;
    }

    public boolean isRightAnswer() {
        return rightAnswer;
    }

    public static class Builder {
        private String battleId;
        private String roundId;
        private boolean rightAnswer = false;

        public Builder battleId(String battleId) {
            this.battleId = battleId;
            return this;
        }

        public Builder roundId(String roundId) {
            this.roundId = roundId;
            return this;
        }

        public Builder isRightAnswer(boolean isRightAnswer) {
            this.rightAnswer = isRightAnswer;
            return this;
        }

        public IsRightAnswerForRoundResult build() {
            return new IsRightAnswerForRoundResult(battleId, roundId, rightAnswer);
        }

    }

}
