package br.com.letscode.moviebattle.service.arg;

public class CreateUserArg {

    private String username;
    private String password;

    private CreateUserArg(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public static class Builder {
        private String username;
        private String password;

        public Builder username(String username) {
            this.username = username;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public CreateUserArg build() {
            return new CreateUserArg(username, password);
        }
    }
}
