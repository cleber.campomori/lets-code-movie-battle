package br.com.letscode.moviebattle.infra.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Component
public class Jwt {

    private static int TOKEN_VALIDITY = 5 * 60 * 60;
    private String key;

    @Autowired
    public Jwt(
            @Value("${app.security.jwt.secret}")
            String key
    ) {
        this.key = key;
    }

    public String fetchUsername(String token) {
        Claims claims = openToken(token);
        return claims.getSubject();
    }

    public LocalDateTime fetchExpiration(String token) {
        Claims claims = openToken(token);
        return claims.getExpiration().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public boolean isExpired(String token) {
        return fetchExpiration(token).isBefore(LocalDateTime.now());
    }

    public String generateForUsername(String username) {
        return Jwts.builder()
                .setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + TOKEN_VALIDITY * 1000))
                .signWith(Keys.hmacShaKeyFor(key.getBytes(StandardCharsets.UTF_8)))
                .compact();
    }

    private Claims openToken(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(Keys.hmacShaKeyFor(key.getBytes(StandardCharsets.UTF_8)))
                .build()
                .parseClaimsJws(token).getBody();
    }

}
