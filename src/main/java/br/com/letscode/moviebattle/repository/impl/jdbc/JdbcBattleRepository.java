package br.com.letscode.moviebattle.repository.impl.jdbc;

import br.com.letscode.moviebattle.model.Battle;
import br.com.letscode.moviebattle.model.BattleStatus;
import br.com.letscode.moviebattle.repository.BattleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Repository
public class JdbcBattleRepository implements BattleRepository {

    private String INSERT = "INSERT INTO battles (id, user_id, created_at, started_at, status) " +
            "VALUES (:id, :user_id,:created_at, :started_at, :status)";
    private String GET_BATTLES_FOR_USER = "SELECT id, user_id, created_at, started_at, number_of_errors, status " +
            "FROM battles " +
            "WHERE user_id = :user_id";

    private String GET_BATTLE_BY_ID = "SELECT id, user_id, created_at, started_at, number_of_errors, status " +
            "FROM battles " +
            "WHERE id = :id";

    private String UPDATE_BATTLE = "UPDATE battles SET started_at = :started_at, status = :status, " +
            "number_of_errors = :number_of_errors " +
            "WHERE id = :id";

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcBattleRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Battle insert(Battle battle) {
        jdbcTemplate.update(INSERT, battleAsInsertParameterMap(battle));
        return battle;
    }

    @Override
    public Collection<Battle> battlesForUser(String userId) {
        return jdbcTemplate.query(GET_BATTLES_FOR_USER, Map.of("user_id", userId), rowMapper);
    }

    @Override
    public Optional<Battle> byId(String battleId) {
        return jdbcTemplate.query(GET_BATTLE_BY_ID, Map.of("id", battleId), rowMapper).stream().findFirst();
    }

    @Override
    public Battle update(Battle battle) {
        jdbcTemplate.update(UPDATE_BATTLE, battleAsUpdateParameterMap(battle));
        return battle;
    }

    private RowMapper<Battle> rowMapper = (rs, rowNum) -> new Battle.Builder()
            .id(rs.getString("id"))
            .createdAt(rs.getTimestamp("created_at").toLocalDateTime())
            .startedAt(rs.getTimestamp("started_at") != null ? rs.getTimestamp("started_at").toLocalDateTime() : null)
            .numberOfErrors(rs.getInt("number_of_errors"))
            .status(BattleStatus.valueOf(rs.getString("status")))
            .build();

    private Map<String, Object> battleAsInsertParameterMap(Battle battle) {
        HashMap<String, Object> parameters = new HashMap();
        parameters.put("id", battle.getId());
        parameters.put("user_id", battle.getUserId());
        parameters.put("created_at", battle.getCreatedAt());
        parameters.put("started_at", battle.getStartedAt().orElse(null));
        parameters.put("status", battle.getStatus().name());
        return parameters;
    }

    private Map<String, Object> battleAsUpdateParameterMap(Battle battle) {
        HashMap<String, Object> parameters = new HashMap();
        parameters.put("id", battle.getId());
        parameters.put("started_at", battle.getStartedAt().orElse(null));
        parameters.put("number_of_errors", battle.getNumberOfErrors());
        parameters.put("status", battle.getStatus().name());
        return parameters;
    }

}
