package br.com.letscode.moviebattle.repository;

import br.com.letscode.moviebattle.model.MovieBattleUser;

import java.util.Optional;

public interface UserRepository {

    Optional<MovieBattleUser> byUsername(String username);

    MovieBattleUser insert(MovieBattleUser movieBattleUser);
}
