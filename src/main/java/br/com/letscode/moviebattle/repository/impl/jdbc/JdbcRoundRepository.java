package br.com.letscode.moviebattle.repository.impl.jdbc;

import br.com.letscode.moviebattle.model.Round;
import br.com.letscode.moviebattle.model.RoundOption;
import br.com.letscode.moviebattle.model.RoundStatus;
import br.com.letscode.moviebattle.repository.RoundRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class JdbcRoundRepository implements RoundRepository {

    private String INSERT_ROUND = "INSERT INTO rounds (id, battle_id, status, created_at, answered_at) " +
            "VALUES (:id, :battle_id, :status, :created_at, NULL)";
    private String INSERT_ROUND_OPTION = "INSERT INTO round_options (id, round_id, name, description, is_right) " +
            "VALUES (:id, :round_id, :name, :description, :is_right)";
    private String GET_ROUND_BY_ID = "SELECT r.id AS r_id, r.battle_id AS r_battle_id, r.status AS r_status, " +
            "r.created_at AS r_created_at, r.answered_at AS r_answered_at, " +
            "o.id AS o_id, o.round_id AS o_round_id, o.name AS o_name, o.description AS o_description, " +
            "o.is_right AS o_is_right " +
            "FROM rounds r " +
            "INNER JOIN round_options o ON o.round_id = r.id " +
            "WHERE r.id = :id";

    private String GET_ROUNDS_BY_BATTLE_ID = "SELECT r.id AS r_id, r.battle_id AS r_battle_id, r.status AS r_status, " +
            "r.created_at AS r_created_at, r.answered_at AS r_answered_at, " +
            "o.id AS o_id, o.round_id AS o_round_id, o.name AS o_name, o.description AS o_description, " +
            "o.is_right AS o_is_right " +
            "FROM rounds r " +
            "INNER JOIN round_options o ON o.round_id = r.id " +
            "WHERE r.battle_id = :battle_id";
    private String UPDATE_ROUND = "UPDATE rounds SET status = :status, answered_at = :answered_at WHERE id = :id";

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcRoundRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Transactional
    @Override
    public Round insert(Round round) {
        jdbcTemplate.update(INSERT_ROUND, toRoundInsertParameterMap(round));
        round.getOptions().forEach(option -> {
            jdbcTemplate.update(INSERT_ROUND_OPTION, toRoundOptionInsertParameterMap(option));
        });
        return round;
    }

    @Override
    public Optional<Round> byId(String roundId) {
        return jdbcTemplate.query(GET_ROUND_BY_ID, Map.of("id", roundId), new RoundRsExtractor())
                .stream().findFirst();
    }

    @Override
    public Collection<Round> byBattleId(String battleId) {
        return jdbcTemplate.query(GET_ROUNDS_BY_BATTLE_ID, Map.of("battle_id", battleId), new RoundRsExtractor());
    }

    @Override
    public Round update(Round round) {
        jdbcTemplate.update(UPDATE_ROUND, toRoundUpdateParameterMap(round));
        return round;
    }

    private Map<String, Object> toRoundInsertParameterMap(Round round) {
        return Map.of(
                "id", round.getId(),
                "battle_id", round.getBattleId(),
                "status", round.getStatus().name(),
                "created_at", round.getCreatedAt()
        );
    }

    private Map<String, Object> toRoundOptionInsertParameterMap(RoundOption option) {
        return Map.of(
                "id", option.getId(),
                "round_id", option.getRoundId(),
                "name", option.getName(),
                "description", option.getDescription().substring(0, Math.min(1024, option.getDescription().length() - 1)),
                "is_right", option.isRight()
        );
    }

    private Map<String, Object> toRoundUpdateParameterMap(Round round) {
        return Map.of(
                "status", round.getStatus().name(),
                "answered_at", round.getAnsweredAt().get(),
                "id", round.getId()
        );
    }

    private RowMapper<Round> roundRowMapper = (rs, rowNum) ->
            new Round.Builder()
                    .id(rs.getString("r_id"))
                    .battleId(rs.getString("r_battle_id"))
                    .status(RoundStatus.valueOf(rs.getString("r_status")))
                    .createdAt(rs.getTimestamp("r_created_at").toLocalDateTime())
                    .answeredAt(rs.getTimestamp("r_answered_at") != null
                            ? rs.getTimestamp("r_answered_at").toLocalDateTime()
                            : null)
                    .build();

    private RowMapper<RoundOption> roundOptionRowMapper = (rs, rowNum) ->
            new RoundOption.Builder()
                    .id(rs.getString("o_id"))
                    .name(rs.getString("o_name"))
                    .description(rs.getString("o_description"))
                    .roundId(rs.getString("o_round_id"))
                    .isRight(rs.getBoolean("o_is_right"))
                    .build();

    private class RoundRsExtractor implements ResultSetExtractor<List<Round>> {

        private List<Round> rounds = new ArrayList<>();

        @Override
        public List<Round> extractData(ResultSet rs) throws SQLException, DataAccessException {
            while (rs.next()) {
                String roundId = rs.getString("r_id");
                if (rounds.stream().noneMatch(r -> r.getId() == roundId)) {
                    rounds.add(roundRowMapper.mapRow(rs, -1));
                }
                rounds.stream().filter(r -> r.getId() == roundId).findFirst().get()
                        .registerOptions(roundOptionRowMapper.mapRow(rs, -1));
            }
            return rounds;
        }
    }
}
