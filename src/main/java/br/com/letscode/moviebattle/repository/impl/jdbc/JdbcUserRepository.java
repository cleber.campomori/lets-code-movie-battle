package br.com.letscode.moviebattle.repository.impl.jdbc;

import br.com.letscode.moviebattle.model.MovieBattleUser;
import br.com.letscode.moviebattle.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Optional;

@Repository
public class JdbcUserRepository implements UserRepository {

    private NamedParameterJdbcTemplate jdbcTemplate;
    private String GET_BY_USERNAME = "SELECT id, username, password FROM users WHERE username = :username";
    private String INSERT = "INSERT INTO users (id, username, password) VALUES (:id, :username, :password)";

    @Autowired
    public JdbcUserRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Optional<MovieBattleUser> byUsername(String username) {
        return jdbcTemplate.query(GET_BY_USERNAME, Map.of("username", username), rowMapper).stream().findFirst();
    }

    @Override
    public MovieBattleUser insert(MovieBattleUser movieBattleUser) {
        jdbcTemplate.update(INSERT, toInsertMap(movieBattleUser));
        return movieBattleUser;
    }

    private RowMapper<MovieBattleUser> rowMapper = (rs, rowNum) ->
            new MovieBattleUser.Builder()
                    .id(rs.getString("id"))
                    .username(rs.getString("username"))
                    .password(rs.getString("password"))
                    .build();

    private Map<String, Object> toInsertMap(MovieBattleUser movieBattleUser) {
        return Map.of(
                "id", movieBattleUser.getId(),
                "username", movieBattleUser.getUsername(),
                "password", movieBattleUser.getPassword()
        );
    }
}
