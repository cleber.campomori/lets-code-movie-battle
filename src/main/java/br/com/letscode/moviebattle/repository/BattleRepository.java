package br.com.letscode.moviebattle.repository;

import br.com.letscode.moviebattle.model.Battle;

import java.util.Collection;
import java.util.Optional;

public interface BattleRepository {

    Battle insert(Battle battle);

    Collection<Battle> battlesForUser(String userId);

    Optional<Battle> byId(String battleId);

    Battle update(Battle battle);
}
