package br.com.letscode.moviebattle.repository;

import br.com.letscode.moviebattle.model.Round;

import java.util.Collection;
import java.util.Optional;

public interface RoundRepository {

    Round insert(Round round);

    Optional<Round> byId(String roundId);

    Collection<Round> byBattleId(String battleId);

    Round update(Round round);
}
