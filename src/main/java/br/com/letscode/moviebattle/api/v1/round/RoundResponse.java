package br.com.letscode.moviebattle.api.v1.round;

import br.com.letscode.moviebattle.model.Round;
import br.com.letscode.moviebattle.model.RoundStatus;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

public class RoundResponse {
    private String id;
    private RoundStatus status;
    private LocalDateTime createdAt;
    private Optional<LocalDateTime> answeredAt;
    private Collection<RoundOptionResponse> options;

    private RoundResponse(String id, RoundStatus status, LocalDateTime createdAt, Optional<LocalDateTime> answeredAt,
                          Collection<RoundOptionResponse> options
    ) {
        this.id = id;
        this.status = status;
        this.createdAt = createdAt;
        this.answeredAt = answeredAt;
        this.options = options;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RoundStatus getStatus() {
        return status;
    }

    public void setStatus(RoundStatus status) {
        this.status = status;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Optional<LocalDateTime> getAnsweredAt() {
        return answeredAt;
    }

    public void setAnsweredAt(Optional<LocalDateTime> answeredAt) {
        this.answeredAt = answeredAt;
    }

    public Collection<RoundOptionResponse> getOptions() {
        return options;
    }

    public void setOptions(Collection<RoundOptionResponse> options) {
        this.options = options;
    }

    public static class Builder {

        private Round round;

        public Builder round(Round round) {
            this.round = round;
            return this;
        }

        public RoundResponse build() {
            return new RoundResponse(round.getId(), round.getStatus(), round.getCreatedAt(), round.getAnsweredAt(),
                    round.getOptions().stream().map(option -> new RoundOptionResponse.Builder().option(option).build()).collect(Collectors.toList()));
        }
    }

}
