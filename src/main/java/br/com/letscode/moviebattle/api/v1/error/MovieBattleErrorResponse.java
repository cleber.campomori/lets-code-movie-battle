package br.com.letscode.moviebattle.api.v1.error;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public class MovieBattleErrorResponse {
    private String message;
    private String route;
    private String method;
    private String correlationId;
    private String httpStatusCode;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(String httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    private MovieBattleErrorResponse(String message, String route, String method, String correlationId, String httpStatusCode) {
        this.message = message;
        this.route = route;
        this.method = method;
        this.correlationId = correlationId;
        this.httpStatusCode = httpStatusCode;
    }

    public static class Builder {
        private String message;
        private HttpServletRequest request;
        private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        private String correlationId = "";

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder request(HttpServletRequest request) {
            this.request = request;
            return this;
        }

        public Builder httpStatusCode(HttpStatus httpStatus) {
            this.httpStatus = httpStatus;
            return this;
        }

        public Builder correlationId(String correlationId) {
            this.correlationId = correlationId;
            return this;
        }

        public ResponseEntity<MovieBattleErrorResponse> response() {
            return new ResponseEntity(new MovieBattleErrorResponse(
                    message,
                    request.getRequestURI(),
                    request.getMethod(),
                    correlationId,
                    httpStatus.name()
            ), httpStatus);
        }

    }
}
