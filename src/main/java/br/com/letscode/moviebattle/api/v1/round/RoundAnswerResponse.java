package br.com.letscode.moviebattle.api.v1.round;

public class RoundAnswerResponse {
    private boolean isRightAnswer = false;

    public RoundAnswerResponse(boolean isRightAnswer) {
        this.isRightAnswer = isRightAnswer;
    }

    public boolean isRightAnswer() {
        return isRightAnswer;
    }
}
