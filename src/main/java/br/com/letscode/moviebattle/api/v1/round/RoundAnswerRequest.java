package br.com.letscode.moviebattle.api.v1.round;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RoundAnswerRequest {

    @JsonProperty("option_id")
    private String optionId;

    @JsonCreator
    public RoundAnswerRequest(String optionId) {
        this.optionId = optionId;
    }

    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }
}
