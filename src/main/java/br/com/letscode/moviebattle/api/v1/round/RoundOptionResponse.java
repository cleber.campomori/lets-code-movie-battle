package br.com.letscode.moviebattle.api.v1.round;

import br.com.letscode.moviebattle.model.RoundOption;

public class RoundOptionResponse {
    private String id;
    private String name;
    private String description;

    private RoundOptionResponse(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static class Builder {
        private RoundOption option;

        public Builder option(RoundOption option){
            this.option = option;
            return this;
        }

        public RoundOptionResponse build() {
            return new RoundOptionResponse(option.getId(), option.getName(), option.getDescription());
        }

    }
}
