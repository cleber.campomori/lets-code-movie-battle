package br.com.letscode.moviebattle.api.v1.battle;

import br.com.letscode.moviebattle.model.Battle;

import java.time.LocalDateTime;

public class BattleResponse {
    private String id;
    private LocalDateTime createdAt;
    private LocalDateTime startedAt;
    private int numberOfErrors;
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(LocalDateTime startedAt) {
        this.startedAt = startedAt;
    }

    public int getNumberOfErrors() {
        return numberOfErrors;
    }

    public void setNumberOfErrors(int numberOfErrors) {
        this.numberOfErrors = numberOfErrors;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static BattleResponse from(Battle battle) {
        BattleResponse response = new BattleResponse();
        response.setId(battle.getId());
        response.setCreatedAt(battle.getCreatedAt());
        response.setStartedAt(battle.getStartedAt().orElse(null));
        response.setNumberOfErrors(battle.getNumberOfErrors());
        response.setStatus(battle.getStatus().name());
        return response;
    }
}
