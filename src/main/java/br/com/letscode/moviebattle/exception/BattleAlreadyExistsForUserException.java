package br.com.letscode.moviebattle.exception;

public class BattleAlreadyExistsForUserException extends RuntimeException {

    public BattleAlreadyExistsForUserException(String userId) {
        super(String.format("There is a battle in progress for user id %s", userId));
    }
}
