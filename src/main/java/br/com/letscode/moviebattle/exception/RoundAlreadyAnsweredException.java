package br.com.letscode.moviebattle.exception;

public class RoundAlreadyAnsweredException extends RuntimeException {
    public RoundAlreadyAnsweredException(String roundId) {
        super(String.format("The round %s was already answered", roundId));
    }

}
