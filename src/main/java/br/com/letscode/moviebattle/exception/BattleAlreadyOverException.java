package br.com.letscode.moviebattle.exception;

public class BattleAlreadyOverException extends RuntimeException {

    public BattleAlreadyOverException(String battleId) {
        super(String.format("The battle %s is already over", battleId));
    }
}
