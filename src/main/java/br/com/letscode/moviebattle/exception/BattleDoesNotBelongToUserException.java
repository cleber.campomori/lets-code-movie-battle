package br.com.letscode.moviebattle.exception;

public class BattleDoesNotBelongToUserException extends RuntimeException {

    public BattleDoesNotBelongToUserException(String battleId) {
        super(String.format("The battle %s does not belong to current user", battleId));
    }
}
