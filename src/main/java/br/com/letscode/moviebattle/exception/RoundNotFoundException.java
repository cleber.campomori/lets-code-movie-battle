package br.com.letscode.moviebattle.exception;

public class RoundNotFoundException extends RuntimeException {

    public RoundNotFoundException(String roundId) {
        super(String.format("The round %s does not exists", roundId));
    }
}
