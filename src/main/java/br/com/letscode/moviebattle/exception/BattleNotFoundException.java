package br.com.letscode.moviebattle.exception;

public class BattleNotFoundException extends RuntimeException {

    public BattleNotFoundException(String battleId) {
        super(String.format("The battle %s does not exists", battleId));
    }
}
