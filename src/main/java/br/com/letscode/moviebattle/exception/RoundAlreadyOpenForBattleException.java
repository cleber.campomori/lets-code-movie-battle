package br.com.letscode.moviebattle.exception;

public class RoundAlreadyOpenForBattleException extends RuntimeException {
    public RoundAlreadyOpenForBattleException(String battleId) {
        super(String.format("There is an open round for battle %s", battleId));
    }

}
