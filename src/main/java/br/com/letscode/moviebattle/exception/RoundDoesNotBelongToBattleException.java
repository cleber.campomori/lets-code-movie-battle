package br.com.letscode.moviebattle.exception;

public class RoundDoesNotBelongToBattleException extends RuntimeException {

    public RoundDoesNotBelongToBattleException(String battleId, String roundId) {
        super(String.format("The battle %s does not have any round with id %s", battleId, roundId));
    }
}
