CREATE TABLE battles
(
    id VARCHAR(36) NOT NULL PRIMARY KEY,
    user_id VARCHAR(36) NOT NULL DEFAULT '',
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    started_at TIMESTAMP DEFAULT NULL,
    number_of_errors INT DEFAULT 0,
    status VARCHAR(32) NOT NULL DEFAULT FALSE
);

CREATE TABLE rounds
(
    id VARCHAR(36) NOT NULL PRIMARY KEY,
    battle_id VARCHAR(36) NOT NULL,
    status VARCHAR(32) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    answered_at TIMESTAMP DEFAULT NULL
);

ALTER TABLE rounds ADD CONSTRAINT fk_rounds__battles FOREIGN KEY (battle_id) REFERENCES battles(id);

CREATE TABLE round_options
(
    id VARCHAR(36) NOT NULL PRIMARY KEY,
    round_id VARCHAR(36) NOT NULL,
    name VARCHAR(256) NOT NULL,
    description VARCHAR(1024) DEFAULT NULL,
    is_right BOOLEAN DEFAULT FALSE
);

ALTER TABLE round_options ADD CONSTRAINT fk_round_options__rounds FOREIGN KEY (round_id) REFERENCES rounds(id);

CREATE TABLE users
(
    id VARCHAR(36) NOT NULL PRIMARY KEY,
    username VARCHAR(128) NOT NULL,
    password VARCHAR(128) NOT NULL
);
