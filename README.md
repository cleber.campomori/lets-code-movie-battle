# Let's Code Challenge - Movie Battle
Solution for Let's Code Challenge - Movie Battle REST API

## Languages and frameworks
- Java 11;
- Spring Boot/Framework 2.7.0;
- Spring Sleuth (for distributed tracing);
- Spring Data JDBC (for database access);
- Spring Cloud 2021.0.3;
- H2 In-Memory Database 2.1.210;
- Flyway 8.5.11 (for database migrations);
- jJWT Library 0.11.5 (for JSON Web Token functions).

## How to run the project?
There are two main ways to run the project:
- By using the IDE support: in case of *JetBrains IntelliJ*, import the project as a Maven Project and run the `main()` method from `MovieBattleApplication`;
- By using the Maven CLI and running the goal `spring-boot:run`.

In both cases, the database will be automatically generated by running Flyway migrations, but all the tables will be initially empty.

## About access to endpoints
Almost all the endpoints of the application are protected by authentication. So, it will be necessary create a user and execute an authentication call to generate a JWT token. The generated JWT token should be placed into `Authorization` header of the API calls.
It is possible to create a new user by calling the endpoint `HTTP POST /user`. The authentication must be done by calling the endpoint `HTTP POST /auth`. The payload responses can be checked by accessing the OpenAPI documentation.

## OpenAPI documentation
The APIs that are exposed by this project are documented by Using SpringDoc (OpenAPI v3/Swagger). The documentation can be accessed by the route `/swagger-ui/index.html`.

## About movie information
All the movie information is fetched from The Movie Database API (themoviedb.org).

## TODOs
- [ ] Finish unit testing;
- [ ] Implement integration testing;
- [ ] Improve general logging;
- [ ] Create logout endpoint. 